## JavaFXColorChart ##
A JavaFX, Java 8 based application, that generates 16 million RGB and Hex colors and displays them on the UI. The application is useful for web developes, graphics &amp; UI designers 
*Below is a typical screen of the application running, on Windows 7.*

![javacolorchart_shot_007[1].png](https://bitbucket.org/repo/nL7G8e/images/1279356830-javacolorchart_shot_007%5B1%5D.png)

## How to use the application (.jar) / (Running the binaries) ##
A JRE 1.8 or later is required to run the application. Download the zip and unzip dist folder to a convinient folder. 
If everyting is okay double clicking on the .jar file will launch the application. Enjoy

## Setting up the project in Netbeans/Eclipse or any other IDE / (Compiling the sources)##
Clone the sources to a convinient directory & simply point netbeans project browser to that location. Enjoy

## More ScreenShots ##
** 01 **
![picture](https://www.dropbox.com/s/l697qw1pjtz0we8/javacolorchart_shot_002.png?raw=1)
** 02 **
![picture](https://www.dropbox.com/s/qpo20lfdrpp4r6b/javacolorchart_shot_003.png?raw=1)
** 03 **
![picture](https://www.dropbox.com/s/thntjok9xx21d20/javacolorchart_shot_005.png?raw=1)